﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesConsoleApp.Args
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; set; }
    }

}
