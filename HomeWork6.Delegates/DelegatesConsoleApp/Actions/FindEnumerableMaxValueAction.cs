﻿using DelegatesConsoleApp.Extensions;
using DelegatesConsoleApp.Logger;
using DelegatesConsoleApp.Model;

namespace DelegatesConsoleApp.Actions
{
    internal class FindEnumerableMaxValueAction: IAction
    {
        ILogger _logger;

        public FindEnumerableMaxValueAction(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute()
        {
            var items = new List<Item>()
            {
                new Item () {Name = "Item1", Value = 100},
                new Item () {Name = "Item5", Value = 500},
                new Item () {Name = "Item6", Value = 600},
                new Item () {Name = "Item2", Value = 200},
                new Item () {Name = "Item4", Value = 400},
                new Item () {Name = "Item3", Value = 300},
            };

            var item = items.GetMax(t => t.Value);

            _logger.WriteToLog($"Элемент с максимальным значением: {item.Name}  Значение: {item.Value}");
        }
    }
}
