﻿using DelegatesConsoleApp.Args;
using DelegatesConsoleApp.Logger;

namespace DelegatesConsoleApp.Actions
{
    internal class SearchFilesAction: IAction
    {
        ILogger _logger;
        string _directoryPath;
        bool _stop;

        public delegate void ButtonEscHandler(ConsoleKey btn);

        public event EventHandler<FileArgs>? FileFound;
        public event ButtonEscHandler EscHandler;

        public SearchFilesAction (ILogger logger, string directoryPath)
        {
            _logger = logger;
            _directoryPath = directoryPath;
        }

        public void Execute()
        {
            _logger.WriteToLog("Поиск начинается! Вы можете в любой момент остановить его клавишей ESC");

            FileFound += ProcessFile;
            EscHandler += EscButtonPressed;
            _stop = false;

            var files = Directory.GetFiles(_directoryPath, "*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                Thread.Sleep(500);
                FileFound?.Invoke(this, new FileArgs { FileName = file });
                if (Console.KeyAvailable)
                {
                    EscHandler(Console.ReadKey(true).Key);
                    if (_stop)
                    {
                        _logger.WriteToLog("Выполнение отменено!");
                        return;
                    }
                }
            }

            _logger.WriteToLog("Поиск завершен!");
        }

        private void EscButtonPressed(ConsoleKey btn)
        {
            if (btn == ConsoleKey.Escape)
                _stop = true;
        }

        void ProcessFile(object? sender, FileArgs e)
        {
            _logger.WriteToLog($"Найден файл: {e.FileName}");
        }

    }
}
