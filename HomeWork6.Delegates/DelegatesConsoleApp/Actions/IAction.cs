﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesConsoleApp.Actions
{
    internal interface IAction
    {
        public void Execute();
    }
}
