﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesConsoleApp.Logger
{
    internal interface ILogger
    {
        public void WriteToLog(string message);
    }
}
