﻿
using DelegatesConsoleApp.Actions;
using DelegatesConsoleApp.Logger;

namespace DelegatesConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var logger = new ConsoleLogger();

            try
            {
                logger.WriteToLog("Шаг 1. Поиск максимального числа");

                new FindEnumerableMaxValueAction(logger).Execute();

                Console.WriteLine("Шаг 2. Анализ директории на предмет файлов");

                var isDirectoryExists = false;
                var directoryPath = "";

                do
                {
                    logger.WriteToLog("Введите путь к директории");
                    directoryPath = Console.ReadLine();

                    isDirectoryExists = Directory.Exists(directoryPath);
                    if (!isDirectoryExists)
                    {
                        logger.WriteToLog("Директория по указанному пути не найдена!");
                    }
                }
                while (!isDirectoryExists);

                new SearchFilesAction(logger, directoryPath).Execute();


            }
            catch(Exception ex)
            {
                logger.WriteToLog(ex.ToString());
            }
            finally
            {
                Console.ReadKey();
            } 
        }
    }
}