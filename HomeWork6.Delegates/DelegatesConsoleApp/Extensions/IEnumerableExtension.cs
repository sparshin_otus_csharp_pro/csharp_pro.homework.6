﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesConsoleApp.Extensions
{
    public static class IEnumerableExtension
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {

            var enumerator = e.GetEnumerator();
            enumerator.MoveNext();
            var maxElement = enumerator.Current;

            foreach(var item in e)
            {
                if(getParameter(item) > getParameter(maxElement))
                {
                    maxElement = item;
                }
            }

            return maxElement;
        }
    }
}
